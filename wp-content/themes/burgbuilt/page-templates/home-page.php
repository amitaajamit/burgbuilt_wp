<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php 
        /*
         * Include the home slider.
         */
        get_template_part( 'page-templates/home-slider' );
    ?>
    <?php if($wp_query->have_posts()){
		 //Start the Loop
		 while(have_posts()) : the_post(); {
			 ?>
		 

<div class="content-section">
            <div class="container">
            <?php  
			/*
			 *  Get the content description of the page
			 */
			    echo get_the_content();
			?>
                <h2 class="text-center">Welcome to <span class="semi-bold uppercase">BURGBUILT</span></h2>
                <p class="text-center">We are a new and vibrant company ready to make a positive mark on the building industry, with <br>twelve years experience in architecturally designed renovations, extensions and new homes. <br>We understand how to dissect, build and rebuild houses to a very high standard.</p>
                <div class="img-thumb-section">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="project-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-1.png" class="img-responsive" alt="thumbnail">
                                <div class="project-details">
                                    <h5 class="project-name">Project name</h5>
                                    <p>A little bit about this project <br>a line or 2 and its location</p>
                                    <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="project-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-2.png" class="img-responsive" alt="thumbnail">
                                <div class="project-details">
                                    <h5 class="project-name">Project name</h5>
                                    <p>A little bit about this project <br>a line or 2 and its location</p>
                                    <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="project-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                <div class="project-details">
                                    <h5 class="project-name">Project name</h5>
                                    <p>A little bit about this project <br>a line or 2 and its location</p>
                                    <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-center">We are constantly developing our services, to meet the needs of customers. We believe our ability to offer <br> a one stop shop for all building requirements represents the highest standards for our clients.</p>
                <h3 class="text-center">Have a project that we can collaborate on? <br> <a href="contact.html">Get in touch with us for a free quote</a></h3>
            </div>
        </div>
<?php

get_footer();?>
