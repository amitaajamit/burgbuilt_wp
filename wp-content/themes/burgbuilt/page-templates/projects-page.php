<?php
/**
 * Template Name: projects-page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="page-title-block">
            <div class="container title-block">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <h1>PROJECTS</h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <ul class="list-inline text-right">
                        <li><p>Call us on <a href="#">1300 555 363</a></p></li>
                        <li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb-black.png" alt="facebook"></a></li>
                        <li><a href=""><img src="<?php bloginfo('template_url'); ?>/img/twt-black.png" alt="twitter"></a></li>
                    </ul>
                </div>
            </div>
        </div>
 <div class="content-section">
            <div class="container">
                <div class="project-gallery-block">
                      <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#category-1" aria-controls="category-1" role="tab" data-toggle="tab">Category 1</a></li>
                        <li role="presentation"><a href="#categor-2" aria-controls="categor-2" role="tab" data-toggle="tab">Category 2</a></li>
                        <li role="presentation"><a href="#category-3" aria-controls="category-3" role="tab" data-toggle="tab">Category 3</a></li>                    
                    </ul>

                      <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="category-1">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-1.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-2.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="categor-2">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-1.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-2.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="category-3">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-1.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-2.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="project-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/img/home-thumb-3.png" class="img-responsive" alt="thumbnail">
                                        <div class="project-details">
                                            <h5 class="project-name">Project name</h5>
                                            <a class="btn btn-default btn-sm project-btn" href="#" role="button">more info</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
<?php

get_footer();
