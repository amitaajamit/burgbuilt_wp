<?php
/**
 * Template Name: projects_single-page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="page-title-block">
            <div class="container title-block">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <h1>PROJECTS name</h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <ul class="list-inline text-right">
                        <li><p>Call us on <a href="#">1300 555 363</a></p></li>
                        <li><a href="#"><img src="img/fb-black.png" alt="facebook"></a></li>
                        <li><a href=""><img src="img/twt-black.png" alt="twitter"></a></li>
                    </ul>
                </div>
            </div>
        </div>
 <div class="content-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <h5 class="text-left">Project name</h5>
                        <p class="list-points clearfix"><span class="semi-bold">Client:</span> Client name</p>
                        <p class="list-points clearfix"><span class="semi-bold">Client:</span> Client name</p>
                        <p class="list-points clearfix"><span class="semi-bold">Client:</span> Client name</p>
                        <p class="list-points clearfix"><span class="semi-bold">Client:</span> Client name</p>
                        <p class="list-points clearfix"><span class="semi-bold">Description:</span></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                        <div id="carousel-example-generic" class="carousel slide slider-section" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <div class="item active">
                              <img src="img/home-banner.jpg" alt="banner">
                            </div>
                            <div class="item">
                              <img src="img/home-banner.jpg" alt="banner">
                              
                            </div>
                            <div class="item">
                              <img src="img/home-banner.jpg" alt="banner">
                              
                            </div>
                            <div class="item">
                              <img src="img/home-banner.jpg" alt="banner">
                              
                            </div>
                            <div class="item">
                              <img src="img/home-banner.jpg" alt="banner">
                              
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php

get_footer();
