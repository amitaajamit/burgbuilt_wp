<?php
/**
 * The template part for displaying home slider
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php
		if( have_rows('slider') ):
			$j = $k = 1;
	?>            
  <div id="carousel-example-generic" class="carousel slide slider-section" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
		<?php
            while( have_rows('slider') ): the_row();
        ?>
        <li data-target="#carousel-example-generic" data-slide-to="<?=$k-1?>" <?php echo (($k == 1) ? ' class="active"' : ''); ?>></li>
		<?php
				$k++;
        	endwhile;
        ?>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
		<?php
            while( have_rows('slider') ): the_row();
                // vars
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $image = get_sub_field('image');
        ?>
        <div class="item <?php echo (($j == 1) ? ' active' : ''); ?>">
          <img src="<?php echo $image ?>" alt="banner">
          <div class="carousel-caption">
            <div class="container title-block">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <h1><?php echo $title ?></h1>
                    <?php echo $description ?>
                    <a class="btn btn-default project-btn" href="<?php bloginfo( 'url' ); ?>/projects/" role="button">VIEW PROJECT</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <ul class="list-inline text-right">
                        <?php $phone = get_field('phone','options'); if(!empty($phone)){ ?><li><p>Call us on <a href="<?php the_field('phone','options'); ?>" target="_blank">1300 555 363</a></p></li><?php } ?>
						<?php $facebook = get_field('facebook','options'); if(!empty($facebook)){ ?><li><a href="<?php the_field('facebook','options'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/fb.png" alt="facebook"></a></li><?php } ?>
						<?php $twitter = get_field('twitter','options'); if(!empty($twitter)){ ?><li><a href="<?php the_field('twitter','options'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/twt.png" alt="twitter"></a></li><?php } ?>
                    </ul>
                </div>
            </div>
          </div>
        </div>
		<?php
				$j++;
        	endwhile;
        ?>
      </div>
  </div>
    <?php endif; ?>

