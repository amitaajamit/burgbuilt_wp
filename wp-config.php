<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'burgbuilt_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0krlKu+hT  m>r4:?9K+coyrx<xyncfm#91-@.JEZKDl]cbt)1{.<[BR*<WD%nT[');
define('SECURE_AUTH_KEY',  '_a!@z=I,V.gq:b@{H2v[=i ]TE3-Fo}hbb=(fM?8Hv,7*B2mHG:1EC@J|,twA{x3');
define('LOGGED_IN_KEY',    'du`RSFHE0UO`mzI~2q`p_)%nK2@ nm&V;>?PM<K2MIoe:B>56iuCTo[ZUF#[eWxD');
define('NONCE_KEY',        'k>$G3m9svPL*QwpG-]AvlB5-h%a0x3u~sAxMpaC]qwTCV_@p!o`(;]ZXrkpy%N{J');
define('AUTH_SALT',        '>;CX87)@,&&e8TcZd1FCZySPXiS]$dJ=n^DHmuG?%>%c;+oIyRUM^s`S-fRi<~d4');
define('SECURE_AUTH_SALT', 'fo~idqNFk(Drk{9]^|euk0lhD3)f7Zt]kEi,h51I:7j$!Rki`.>IV<J&^_2x1`l@');
define('LOGGED_IN_SALT',   '<|%KALXhqsH!:=|/j]=br>p+SL5_a7Dn+L5TaOiORA+!F5!Pl/kAcValbEI[Ed6u');
define('NONCE_SALT',       '6tZ:`YO~6%XL,VnLLf}LPnBhY2G>KhZ8s,BT^y/mel[%<<9V(_YDtdu3(WWoh3>;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
